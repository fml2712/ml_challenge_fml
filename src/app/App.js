import React, { Component } from 'react';
import { SearchBar } from './components/common/searchBar';

class App extends Component {

    render(){
        return (
            <div className='container-fluid search-container'>
                <h1>Home</h1> 
                <SearchBar/>
            </div>
        )
    }
}

export default App;