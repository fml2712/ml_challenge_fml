import React, { Component } from 'react';
import { Breadcrumb } from '../common/breadcrumb';
import { ItemListDetails } from './itemListDetails';
import { Item } from './item';

//import './styles/searchBar.scss';

export class ItemsList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            itemShow: [],
            itemsShow: []
        };
        this.showItem = this.showItem.bind(this);
    }

    getResponse(itemShow){
        this.setState({itemShow: itemShow});
    }

    showItem(){
        const prd_item =  e.currentTarget.dataset.id;
        if (prd_item != '') {
            fetch('/api/item/' + prd_item, {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => 
                    this.setState({
                        itemShow: data.details
                    }),
                    console.log("y aca entra a limpiar")
                )
                .catch(err => console.error(err));
        } else {
            this.setState({
                itemShow: ''
            })
        }
    }

    render(){

        console.log(this.state.itemShow);

        let items = this.props.products.map((item) =>{
            return <ItemListDetails item={item} key={item.id} callback={this.getResponse.bind(this)} />;
        });

        let cat = this.props.categories;
        var category = ( cat != undefined ) ? <Breadcrumb category={cat} /> : '';
        
        return (
            <div className='container-fluid search-container'>
                { category }
                { items }
                <Item item={this.state.itemShow} key={this.state.itemShow.id} />
            </div>
        )
    }
}