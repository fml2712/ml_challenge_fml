import React, { Component } from 'react';
import { Item } from '../items/item';

//import './styles/searchBar.scss';

export class ItemListDetails extends React.Component {

    constructor(props) {
        super(props);
    }

    calc(prd_item){
        this.props.callback(prd_item);
    }

    render(){

        return (
            <div>
				<li>
                        <div className="item-list" onClick={() => this.calc(this.props.item.id)}>
								<div className="item-img">
									<img src="img/iphone.jpg" />
								</div>
								<div className="item-info">
									<div className="info">
										<div className="item-price">
											<span className="price_symbol">$</span>
											<span className="price">{ this.props.item.price.amount }</span>
										</div>
										<div className="item_shipping">
											<span><img src={ this.props.item.picture } /></span>
										</div>
										<h2 className="item_title">{ this.props.item.title }</h2>
									</div>
								</div>
								<div className="item-dir">
									<div className="dir">
										<span>{ this.props.item.state }</span>
									</div>
								</div>
								<div className="line-border"></div>
						</div>
                </li>
            </div>
        )
    }
}