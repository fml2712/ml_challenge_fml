import React, { Component } from 'react';
import '../../styles/searchBar.scss';
import { ItemsList } from '../items/itemsList';

export class SearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            search: '',
            items: [],
            categories: []
        };
        this.changeSearch = this.changeSearch.bind(this);
        this.searchItem = this.searchItem.bind(this);
    }

    searchItem(e){
        e.preventDefault();
        const item = this.state.search;
        if (item != '') {
            fetch('/api/items/' + item, {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => 
                    this.setState({
                        search: '',
                        items: data.items,
                        categories: data.categories
                    })
                )
                .catch(err => console.error(err));
        } else {
            this.setState({
                search: '',
                items: [],
                categories: []
            })
        }
    }

    changeSearch(e){
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }

    render(){
        return (
            <div className='container-fluid search-container'>
                <div className='row justify-content-center'>
                            <form className='input-group d-flex' type='submit' onSubmit={this.searchItem}>
                                <input
                                    name="search"
                                    placeholder="Nunca dejes de buscar"
                                    type="text"
                                    onChange={this.changeSearch}
                                    value={this.state.search}
                                    />
                                <button className='search-button' alt="Buscar">
                                        Buscar
                                </button>
                            </form>
                </div>
                <div className='col-sm-auto align-self-center text-center'>
                    <ItemsList categories={this.state.categories[0]} products={this.state.items} />
                </div>
            </div>
        )
    }
}