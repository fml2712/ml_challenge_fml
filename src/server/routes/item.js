const express = require('express');
const router = express.Router();
const rp = require('request-promise');

/* GET item page. */
router.post('/', (req, res, next) => {
    const itemId = req.body.item.id;
    console.log(req.body.item.id);
    const items = {
        uri: 'https://api.mercadolibre.com/items/' + itemId,
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };
     
    rp(items)
        .then(function (item) {
            const itemDetails = {
                author: {
                    name: 'Facundo',
                    lastname: 'Lemos'
                },
                details: {
                    id: item.id,
                    title: item.title,
                    price: {
                        currency: item.currency_id,
                        amount: item.amount
                    },
                    picture: item.pictures,
                    condition: item.condition,
                    free_shipping: item.shipping ? item.shipping.free_shipping : false,
                    sold_quantity: item.sold_quantity,
                    description: item.description
                    }
                }  
            //res.send(itemDetails);
            res.json(itemDetails);
        })
        .catch(function (err) {
            console.log('Error', err);
        });
        
});

module.exports = router;