const express = require('express');
const app = express();
const morgan = require('morgan');
const path = require('path');
//var homeRouter = require('./server/routes/index');
//var itemsRouter = require('./server/routes/items');
//var itemRouter = require('./server/routes/item');


//app.use('/', homeRouter);
//app.use('/items/:query', itemsRouter);
//app.use('/item/:id', itemRouter);

//Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Routes
app.use('/api/items/:query', require('./server/routes/items'));
app.use('/api/item/:id', require('./server/routes/item'));


//Static files
app.use(express.static(path.join(__dirname, 'public')));

//Settings
app.set('port', process.env.PORT || 3000);

//Starting the server
app.listen(app.get('port'), () => {
    console.log(`Server started on port ${app.get('port')}`);
});

