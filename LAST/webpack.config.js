const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: './src/app/index.js',
    output: {
        path: __dirname + '/src/public',
        filename: 'bundle.js'
    },
    module:{
        rules:[
            {
                use:'babel-loader',
                test: /\.(js|jsx)$/,
                exclude:/node_module/
            },
            { 
                test: /\.scss$/, 
                loader: [
                  MiniCSSExtractPlugin.loader,
                  "css-loader",
                  'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({  
          filename: 'index.html',
          template: 'src/public/index.html',
          hash: true
        }),
        new MiniCSSExtractPlugin({
            filename: "./css/styles.css",
        })
      ],
}