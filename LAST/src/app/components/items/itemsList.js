import React, { Component } from 'react';
import { Breadcrumb } from '../common/breadcrumb';
import { ItemListDetails } from './itemListDetails';
//import './styles/searchBar.scss';

export class ItemsList extends React.Component {

    render(){
        let items = this.props.products.map((item) =>{
            return <ItemListDetails item={item} key={item.id} />
        });

        let cat = this.props.categories;
        var category = ( cat != undefined ) ? <Breadcrumb cat={cat} /> : '';

        return (
            <div className='container-fluid search-container'>
                <h1>Categorias mas items</h1>
                { category }
                { items }
            </div>
        )
    }
}