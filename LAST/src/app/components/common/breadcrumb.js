import React, { Component } from 'react';
//import './styles/searchBar.scss';

export class Breadcrumb extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cat: ''
        };
    }

    componentWillMount(){
        this.setState({
            cat: this.props.cat
        })
    }

    render(){
        return (
            <div className='container-fluid search-container'>
                <h1>{this.state.cat}</h1>
            </div>
        )
    }
}