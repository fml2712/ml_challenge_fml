var express = require('express');
const router = express.Router();
const rp = require('request-promise');

/* GET Items page. */
router.post('/', (req, res, next) => {
  const query = req.body.search;
  const allItems = {
      uri: 'https://api.mercadolibre.com/sites/MLA/search?q=' + query,
      headers: {
          'User-Agent': 'Request-Promise'
      },
      json: true // Automatically parses the JSON string in the response
  };
   
  rp(allItems)
      .then(function (items) {
              var items = items.results.slice(0,4);
              var category = items.map((item) => {
                return item.category_id
              });
              var itemsDetails = items.map((item) => {
              const decimals = +(item.price%1).toFixed(2).substring(2);
              return {
                  id: item.id,
                  title: item.title,
                  price: {
                      currency: item.currency_id,
                      amount: item.price,
                      decimals: decimals
                  },
                  picture: item.thumbnail,
                  condition: item.condition,
                  free_shipping: item.shipping ? item.shipping.free_shipping : false,
                  state: item.seller_address.state.name
              }
          });
        const itemsList = {
            author: {
                name: 'Facundo Matias',
                lastname: 'Lemos'
            },
            items: itemsDetails,
            categories: category
        }
          res.json(itemsList);
      })
      .catch(function (err) {
          console.log('Error', err);
      });
  
});



module.exports = router;
